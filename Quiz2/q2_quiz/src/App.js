
import { Routes, Route } from "react-router-dom";
import './App.css';
import Header from './components/Header'
import AboutUsPage from './pages/AboutUsPage';
import NumberPage from './pages/NumberPage';


function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
          <Route path="about" element ={
              <AboutUsPage/>
          }/>

          <Route path="/" element ={
              <NumberPage/>
          }/>
      </Routes>
    </div>
  );
}

export default App;
