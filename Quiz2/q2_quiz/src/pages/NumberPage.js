import NumberResult from '../components/NumberResult.js'
import {useState} from "react";
import Button from '@mui/material/Button';

function NumberPage() {

  
    
    const [ name, setName ] = useState("");
    const [numberResult, setNumberResult] = useState("");
    const [ translateResult, setTranslateResult ] = useState("");


    const [nn, setNumber] = useState("");

   

     function calculatenumber(){
         let n =parseFloat(nn);
         let x = 2 ;
         let num = n%x;
         setNumberResult(num);
            if(num==0){
                setTranslateResult("เลขคู่");
            }else if(num==1){
                setTranslateResult("เลขคี่");    
            
            }
        }
    return(
    <div align = "left">
        <div align = "center">
            <br/>
            <h1>เว็บคำนวณ</h1>
  
            <h2>ชื่อ : <input type = "text" value={name} onChange={(e) => { setName(e.target.value);}}/>
            </h2>
   
            <h3>ใส่เลข <input type = "text" value={nn} onChange={(e) =>{setNumber(e.target.value);}}/>
            </h3>


            <br />
          
            <Button variant="contained" onClick={(e) => {calculatenumber()}}>คำนวณ</Button>
            
            <br/>
            <br/>
     

            { NumberResult != 0 && 
                <div>
                    <h3>ผลคำนวณออกแล้วจ้า</h3>
                    <NumberResult
                        name ={ name }
                        num = { numberResult }
                        result = {translateResult}
                    />
                </div>
            }
                    
        </div>
    </div>
    );
   }


export default NumberPage;